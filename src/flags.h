#pragma once

#include <stdbool.h>

void flags_string(const char *name, const char **dest);
void flags_bool(const char *name, bool *dest);
void flags_parse(int argc, const char **argv);
