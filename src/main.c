#include "attribs.h"
#include "client.h"
#include "server.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Command management

typedef int (*command_t)(int, const char **);

typedef struct command_def {
  const char *name;
  command_t command;
} command_def;

command_def commands[] = {{"client", client}, {"server", server}, {NULL, NULL}};

int main(int argc, const char **argv) {
  // find command name
  const char *command_name = NULL;
  int command_position = 0;

  for (int i = 1; i < argc; i++) {
    const char *arg = argv[i];
    unsigned long len = strlen(arg);

    if (len == 0) {
      // ??
      continue;
    }

    if (strcmp(arg, "--") == 0) {
      // command name cannot appear after '--'
      break;
    }

    if (arg[0] == '-') {
      // not a command name
      continue;
    }

    // we found the first non-flag arg!
    // it is our command-name.
    command_name = arg;
    command_position = i;
    break;
  }

  if (command_name == NULL) {
    fprintf(stderr, "Missing command\n");
    exit(EXIT_FAILURE);
  }

  // remove command-name from args before passing
  // them down to the command itself.
  for (int i = command_position; i < argc; i++) {
    argv[i] = argv[i + 1];
  }
  argc--;

  // invoke command
  for (int i = 0;; i++) {
    command_def *d = &commands[i];
    if (d->name == NULL) {
      // no more commands
      break;
    }
    if (strcmp(d->name, command_name) == 0) {
      exit(d->command(argc, argv));
    }
  }

  // we didn't find a command matching the command-name
  fprintf(stderr, "Unknown command: %s\n", command_name);
  exit(EXIT_FAILURE);
}
