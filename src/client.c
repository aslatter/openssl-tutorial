#include "client.h"
#include "attribs.h"
#include "flags.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/ssl.h>

// Forward declarations

BIO *connect_insecure(const char *addr);
BIO *connect_secure(SSL_CTX *ctx, const char *addr);

void error_and_exit(const char *message);
void http_demo(BIO *bio, const char *host, const char *resource);
int write_harder(BIO *bio, const char *buff, int len);
int read_harder(BIO *bio, char *buff, int len);

int client(int argc, const char **argv) {
  // flags

  const char *addr = "www.google.com:80";
  const char *host = "www.google.com";
  const char *resource = "/foo_bar";
  bool secure = false;

  flags_string("addr", &addr);
  flags_string("host", &host);
  flags_string("resource", &resource);
  flags_bool("secure", &secure);
  flags_parse(argc, argv);

  SSL_CTX *ctx = NULL;
  BIO *bio = NULL;

  if (secure) {
    const SSL_METHOD *ssl_method = TLS_client_method();
    if (ssl_method == NULL) {
      error_and_exit("Error getting SSL method ...");
    }

    ctx = SSL_CTX_new(ssl_method);
    if (ctx == NULL) {
      error_and_exit("Error creating SSL context ...");
    }

    bio = connect_secure(ctx, addr);
  } else {
    bio = connect_insecure(addr);
  }

  http_demo(bio, host, resource);

  // close connection and free resources associated with BIO object
  BIO_free_all(bio);
  bio = NULL;

  if (ctx != NULL) {
    SSL_CTX_free(ctx);
  }

  return EXIT_SUCCESS;
}

BIO *connect_insecure(const char *addr) {
  // insecure connection (but using OpenSSL libs)
  BIO *bio = BIO_new_connect(addr);
  if (bio == NULL) {
    fprintf(stderr, "Failed to connect to '%s'\n", addr);
    exit(EXIT_FAILURE);
  }

  if (BIO_do_connect(bio) <= 0) {
    fprintf(stderr, "Not connected to '%s'\n", addr);
    exit(EXIT_FAILURE);
  }
  return bio;
}

BIO *connect_secure(SSL_CTX *ctx, const char *addr) {
  BIO *bio = BIO_new_ssl_connect(ctx);
  if (bio == NULL) {
    error_and_exit("Error creating SSL BIO ...");
  }

  // grab and prep SSL session
  SSL *ssl = NULL;
  BIO_get_ssl(bio, &ssl);
  SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);
  BIO_set_conn_hostname(bio, addr);

  // try and connect!
  if (BIO_do_connect(bio) <= 0) {
    BIO_free_all(bio);
    SSL_CTX_free(ctx);
    error_and_exit("Could not connect ...");
  }

  // print some debugging info
  // printf("SSL version: %s\n", SSL_get_version(ssl));
  // printf("SSL cipher: %s\n", SSL_get_cipher_name(ssl));
  // puts("");

  // at this point we have an encrypted connection.
  // We can use BIO io methods to read and write plain
  // text, but it will go across encrypted.
  //
  // However we haven't performed any host-cert verifications
  // or anything like that.
  //
  // TODO - various verifications go here

  return bio;
}

void http_demo(BIO *bio, const char *host, const char *resource) {
  char buff[100 + 1];
  int len;

  len = snprintf(buff, 100, "GET %s HTTP/1.1\r\n", resource);
  write_harder(bio, buff, len);

  len = snprintf(buff, 100, "Host: %s\r\n", host);
  write_harder(bio, buff, len);

  len = snprintf(buff, 100, "User-Agent: X-Demo-Agent\r\n");
  write_harder(bio, buff, len);

  len = snprintf(buff, 100, "Connection: close\r\n");
  write_harder(bio, buff, len);

  write_harder(bio, "\r\n", 2);

  // dump response
  while ((len = read_harder(bio, buff, 100)) > 0) {
    fwrite(buff, 1, len, stdout);
  }
  fflush(stdout);
}

int write_harder(BIO *bio, const char *buff, int len) {
  int orig_len = len;
  while (len > 0) {
    int w = BIO_write(bio, buff, len);
    if (w <= 0) {
      if (BIO_should_retry(bio)) {
        continue;
      }
      return w;
    }
    len -= w;
    buff += w;
  }
  return orig_len;
}

int read_harder(BIO *bio, char *buff, int len) {
  int orig_len = len;
  while (len > 0) {
    int r = BIO_read(bio, buff, len);
    if (r == 0) {
      // EOF
      return orig_len - len;
    }
    if (r < 0) {
      if (BIO_should_retry(bio)) {
        continue;
      }
      return r;
    }
    len -= r;
    buff += r;
  }
  return orig_len - len;
}

void error_and_exit(const char *message) {
  fprintf(stderr, "%s\n", message);
  ERR_print_errors_fp(stderr);
  exit(EXIT_FAILURE);
}
