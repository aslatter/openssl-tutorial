#include "flags.h"

#include <ctype.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// forward declarations

static void check_flag_name(const char *name);
struct flag_def;
static void process_flag(int argc, const char **argv, int *argc_pos,
                         struct flag_def *d, bool is_inline,
                         const char *inline_str);
static void process_flag_string(int argc, const char **argv, int *argc_pos,
                                struct flag_def *d, bool is_inline,
                                const char *inline_str);
static void process_flag_bool(struct flag_def *d, bool is_inline,
                              const char *inline_str);
static bool parse_bool_val(const char *name, const char *val);
static void usage(bool is_error);

// local types and globals

struct flag_def {
  // name of flag
  const char *name;
  // if a string, where we should store the value
  const char **string_dest;
  // if a boolean, where we should store the value
  bool *bool_dest;
};

// allow 100 flags
#define max_flags 100
struct flag_def flag_set[max_flags];
int next_flag = 0;

#define max_flag_len 255

// define a new string-valued flag
void flags_string(const char *name, const char **dest) {
  check_flag_name(name);

  flag_set[next_flag].name = name;
  flag_set[next_flag].string_dest = dest;
  next_flag++;
}

// define a new string-valued flag
void flags_bool(const char *name, bool *dest) {
  check_flag_name(name);

  flag_set[next_flag].name = name;
  flag_set[next_flag].bool_dest = dest;
  next_flag++;
}

static void check_flag_name(const char *name) {
  if (next_flag >= max_flags) {
    fprintf(stderr, "Too many flag definitions: '%s'\n", name);
    exit(EXIT_FAILURE);
  }
  if (strlen(name) > max_flag_len) {
    fprintf(stderr, "Flag definition too long: '%s'\n", name);
    exit(EXIT_FAILURE);
  }
}

// process all defined flags using the values
// in argc & argv. Once this returns all destinations
// for flag-values will be non-NULL.
void flags_parse(int argc, const char **argv) {
  // in theory we should modify argc and argv, but
  // I don't need that at the moment.

  for (int i = 1; i < argc; i++) {
    const char *arg = argv[i];
    unsigned long len = strlen(arg);

    if (strcmp(arg, "--") == 0) {
      // done processing flags
      break;
    }

    if (len == 0) {
      // ??
      continue;
    }

    if (arg[0] != '-') {
      // not a flag
      continue;
    }

    // Possibilties:
    // -flag=value
    // --flag=value
    // -flag value
    // --flag value

    // trim off leading '-' or '--'
    arg++;
    len--;
    if (len > 0 && arg[0] == '-') {
      arg++;
      len--;
    }

    // first determine if we have an in-line value
    bool is_inline = false;
    const char *inline_str = strchr(arg, '=');
    if (inline_str != NULL) {
      is_inline = true;
      inline_str++; // skip over '='
    }

    // next extract flag-name
    char flag_name[max_flag_len + 1];
    if (!is_inline) {
      if (len > max_flag_len) {
        fprintf(stderr, "Flag too long: %s\n", arg);
        exit(EXIT_FAILURE);
      }
      strncpy(flag_name, arg, max_flag_len);
      flag_name[max_flag_len] = '\0';
    } else {
      ptrdiff_t inline_pos = inline_str - arg - 1;
      if (inline_pos > max_flag_len) {
        fprintf(stderr, "Flag too long: %s\n", arg);
        exit(EXIT_FAILURE);
      }
      strncpy(flag_name, arg, inline_pos);
      flag_name[inline_pos] = '\0';
    }

    // special-case for 'help' flag
    if (strcmp(flag_name, "help") == 0) {
      usage(false);
      exit(EXIT_SUCCESS);
    }

    // next find flag-definition and assign-value
    bool found_flag = false;
    for (int j = 0; j < next_flag; j++) {
      struct flag_def *d = &flag_set[j];
      if (strcmp(d->name, flag_name) != 0) {
        continue;
      }

      found_flag = true;

      process_flag(argc, argv, &i, d, is_inline, inline_str);

      break;
    }
    if (!found_flag) {
      usage(true);
      fprintf(stderr, "Unknown flag: '%s'\n", flag_name);
      exit(EXIT_FAILURE);
    }

    // done with one arg!
  }

  // validate we found required arguments
  for (int i = 0; i < next_flag; i++) {
    struct flag_def *d = &flag_set[i];
    if (d->string_dest != NULL &&
        (*d->string_dest == NULL || strlen(*d->string_dest) == 0)) {
      fprintf(stderr, "Flag '-%s' was not specified but is required.\n",
              d->name);
      exit(EXIT_FAILURE);
    }
  }
}

static void process_flag(int argc, const char **argv, int *argc_pos,
                         struct flag_def *d, bool is_inline,
                         const char *inline_str) {

  bool is_bool = d->bool_dest != NULL;
  if (is_bool) {
    process_flag_bool(d, is_inline, inline_str);
  } else {
    process_flag_string(argc, argv, argc_pos, d, is_inline, inline_str);
  }
}

static void process_flag_string(int argc, const char **argv, int *argc_pos,
                                struct flag_def *d, bool is_inline,
                                const char *inline_str) {

  // validate we have a next-arg if we need it
  if (!is_inline) {
    if (*argc_pos + 1 >= argc) {
      fprintf(stderr, "Flag '%s' requires a value\n", d->name);
      exit(EXIT_FAILURE);
    }
  }

  const char *val;
  if (is_inline) {
    val = inline_str;
  } else {
    val = argv[*argc_pos + 1];
    (*argc_pos)++;
  }

  *d->string_dest = val;
}

static void process_flag_bool(struct flag_def *d, bool is_inline,
                              const char *inline_str) {
  if (is_inline) {
    *d->bool_dest = parse_bool_val(d->name, inline_str);
  } else {
    // boolean without an argument is toggled to true
    *d->bool_dest = true;
  }
}

static bool parse_bool_val(const char *name, const char *val) {
  if (strcasecmp(val, "true") == 0) {
    return true;
  }
  if (strcasecmp(val, "false") == 0) {
    return false;
  }
  if (strcmp(val, "1") == 0) {
    return true;
  }
  if (strcmp(val, "0") == 0) {
    return false;
  }

  fprintf(stderr, "Invalid argument for flag '%s', not a boolean: '%s'\n", name,
          val);
  exit(EXIT_FAILURE);
}

static void string_usage(FILE *f, struct flag_def *d) {
  if (*d->string_dest != NULL && strlen(*d->string_dest) > 0) {
    fprintf(f, "\t-%s <%s>\n\t default: %s\n\n", d->name, d->name,
            *d->string_dest);
  } else {
    fprintf(f, "\t-%s <%s>\n\n", d->name, d->name);
  }
}

static void bool_usage(FILE *f, struct flag_def *d) {
  fprintf(f, "\t-%s\n\t default: %s\n\n", d->name,
          *d->bool_dest ? "true" : "false");
}

static void usage(bool is_error) {
  FILE *f = is_error ? stderr : stdout;

  fputs("Available flags:\n", f);
  for (int i = 0; i < next_flag; i++) {
    struct flag_def *d = &flag_set[i];
    if (d->bool_dest != NULL) {
      bool_usage(f, d);
    } else if (d->string_dest != NULL) {
      string_usage(f, d);
    }
  }
  fputs("\t-help\n\t print this message and exit\n\n", f);
}
