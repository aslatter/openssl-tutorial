
CC ?= gcc

CFLAGS := -ggdb -fno-omit-frame-pointer
CFLAGS += -Wall -Wextra -Werror -Wformat=2
CFLAGS += -MMD -MP
CLFAGS += -fsanitize=address -fsanitize=undefined

LDFLAGS := 
LDFLAGS += -fsanitize=address -fsanitize=undefined

LDLIBS := -lssl -lcrypto

ofiles = obj/client.o obj/server.o obj/flags.o

all: bin/main

obj/%.o: src/%.c Makefile
	@ echo " CC $<"
	@ mkdir -p obj
	@ $(CC) $(CFLAGS) -c $< -o $@
.PRECIOUS: obj/%.o

-include $(wildcard obj/*.d)

bin/% : obj/%.o $(ofiles)
	@ echo " LD $@"
	@ mkdir -p bin
	@ $(CC) $(LDFLAGS) -o $@ $< $(ofiles) $(LDLIBS)

clean:
	@ $(RM) -r obj
	@ $(RM) -r bin
.PHONY: clean

format:
	@ clang-format -i src/*.c src/*.h
.PHONY: format

tidy:
	@ clang-tidy --fix -checks=bugprone-\*,-clang-analyzer-security.\* src/*.c src/*.h -- $(CFLAGS)
.PHONY: tidy

