# OpenSSL Tutorial

The OpenSSL project doesn't have good documentation.

This project tracks my attempts at trying to learn how
to use OpenSSL.

Links:

* https://developer.ibm.com/solutions/security/tutorials/l-openssl/
* https://opensource.com/article/19/6/cryptography-basics-openssl-part-1
* https://www.openssl.org/docs/man1.1.0/man7/

# Building

Run *make*. This will build *bin/main*.

The *Makefile* is not production-grade - it's merely what is
convinient to me for learning (i.e. all warnings and sanitizers
enabled).

